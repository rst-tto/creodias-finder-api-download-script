#! /usr/bin/python3

import datetime
import urllib.request, json 
import wget
import subprocess
import os
from pathlib import Path

def refreshToken(script):
	pipe = subprocess.Popen(". %s; env" % script, stdout=subprocess.PIPE, shell=True)
	output = pipe.communicate()[0]
	for line in output.splitlines():
		line = line.decode()
		if line.startswith("KEYCLOAK_TOKEN="):
			token=line[15:]
	print("Token received")
	print("-----------------")
	return token

with urllib.request.urlopen("https://finder.creodias.eu/resto/api/collections/Landsat8/search.json?maxRecords=500&q=Bulgaria&startDate=2017-01-01T00:00:00Z&completionDate=2017-12-31T23:59:59Z&cloudCover=[0,10]&sortParam=startDate&sortOrder=descending&status=all&dataset=ESA-DATASET") as url:
	data = json.loads(url.read().decode())
	features = data['features']
	for result in features:
		date_time_str = result['properties']['completionDate']
		date_time_obj = datetime.datetime.strptime(date_time_str, "%Y-%m-%dT%H:%M:%S.%fZ")
		download_path = "/backupHDD/"+result['properties']['collection']+"/"+str(date_time_obj.year)+"/"+str(date_time_obj.month)+"/"+str(date_time_obj.day)
		print(download_path)
		download_filepath = download_path + "/" + str(result['properties']['title'])
		my_file = Path(download_filepath)
		if not my_file.is_file():	
			os.makedirs(download_path, exist_ok=True)
			try:		
				token = refreshToken('/home/user/Desktop/keyc.sh')
				wget.download(
		                url=result['properties']['services']['download']['url']+"?token="+str(token),
		                out=download_filepath
				)
			except Exception:
				print("Error occurred - skipping file")
		else:
			print("Already downloaded")
